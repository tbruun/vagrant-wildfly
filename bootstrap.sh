#!/usr/bin/env bash

## SETTINGS - you should go through theese settings as a minimum before starting your project.
USER_PASSWORD=superSecretPassword
DATABASE_NAME=mysqlDS

ADD_DATASOURCE=true
DATASOURCE_NAME=$DATABASE_NAME
DATASOURCE_JNDI_NAME=java:/jdbc/$DATABASE_NAME

## to use JMS you need to set WIDLFLY_DEFAULT_CONFIG to "standalone-full.xml" 
## in wildfly-install.sh, or tweak your own configuration for JMS support.
ADD_JMS=true
JMS_QUEUE_NAME=myJmsQueue
JMS_QUEUE_JNDI_NAME=queue/$JMS_QUEUE_NAME

## Install Oracle Java 8
sudo apt-get install -y python-software-properties debconf-utils
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update
echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections
sudo apt-get install -y oracle-java8-installer
sudo apt-get install -y oracle-java8-set-default

## Install mysql
echo mysql-server-5.0 mysql-server/root_password password $USER_PASSWORD  | debconf-set-selections
echo mysql-server-5.0 mysql-server/root_password_again password $USER_PASSWORD | debconf-set-selections
sudo apt-get install -y mysql-server

# Add mysql database to use with wildfly
mysql -uroot -p$USER_PASSWORD -e "CREATE DATABASE $DATABASE_NAME;"
# Import bootstrap SQL
mysql -uroot -p$USER_PASSWORD $DATABASE_NAME < /vagrant/bootstrap.sql

## Install Wildfly 9
sudo chmod a+x /vagrant/wildfly-install.sh
sudo sh /vagrant/wildfly-install.sh

## Add admin user to wildfly
sudo /opt/wildfly/bin/add-user.sh admin $USER_PASSWORD --silent

## Download and deploy mysql connector
sudo wget http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.36.tar.gz -O /tmp/mysql-connector.tar.gz
sudo tar -xvzf /tmp/mysql-connector.tar.gz -C /tmp/
sudo cp /tmp/mysql-connector-java-5.1.36/mysql-connector-java-5.1.36-bin.jar /opt/wildfly/standalone/deployments/mysql-connector-java-5.1.36-bin.jar

# Clean downloaded files from temp folder
sudo rm /tmp/mysql-connector.tar.gz
sudo rm -r /tmp/mysql-connector-java-5.1.36/

## Add datasource to wildfly
if [ "$ADD_DATASOURCE" == "true" ]; then
	sudo /opt/wildfly/bin/jboss-cli.sh --connect --user=admin --password=$USER_PASSWORD --command="\
		data-source add \
			--name=$DATASOURCE_NAME \
			--jndi-name=$DATASOURCE_JNDI_NAME \
			--driver-name=mysql-connector-java-5.1.36-bin.jar_com.mysql.jdbc.Driver_5_1 \
			--connection-url=jdbc:mysql://127.0.0.1:3306/$DATABASE_NAME \
			--user-name=root \
			--password=$USER_PASSWORD"

	echo "Datasource added with name: $DATABASE_NAME at java:/jdbc/$DATABASE_NAME"
fi

## Add JMS queue
if [ "$ADD_JMS" == "true" ]; then
	sudo /opt/wildfly/bin/jboss-cli.sh --connect --user=admin --password=$USER_PASSWORD --command="\
		jms-queue add \
			--queue-address=$JMS_QUEUE_NAME \
			--entries=$JMS_QUEUE_JNDI_NAME \
			subsystem=messaging \
			hornetq-server=default \
			jms-queue=myQueue:read-resource"

	echo "JMS queue added with name: $JMS_QUEUE_NAME at $JMS_QUEUE_JNDI_NAME"
fi
