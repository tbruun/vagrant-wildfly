What is this ?
==============
This is a repository to learn about how to use vagrant, and how to get a portable development evironment with wildfly hooked up with mysql server.

After running the Vagrantfile, you will have a wildfly and mysql server running on a provisioned portable virtual machine. 

Ports will be forwarded to localhost (i.e you can access wildfly from localhost:8080 and admin from localhost:9990)

Installation:
===============
On all systems you need to verify that you have Vt-x/AMD-V hardware acceleration enabled in your BIOS/UEFI settings. Most modern CPUs will support this.
### Linux:

##### Install virtualbox:
    $ sudo apt-get install virtualbox

##### Install dkms package:
    $ sudo apt-get install virtualbox-dkms
_(This package is needed to ensure that the VirtualBox host kernel modules (vboxdrv, vboxnetflt and vboxnetadp) are properly updated if the Linux kernel version changes during the next apt-get upgrade.)_

##### Install vagrant:
	https://www.vagrantup.com/downloads.html
_(Do not vagrant itself from ubuntu package manager, since the version is outdated and may not function properly.)_

### Windows/Mac:
##### Install virtualbox:
	https://www.virtualbox.org/wiki/Downloads 
##### Install vagrant:
    https://www.vagrantup.com/downloads.html

(you need to restart your windows box after install)

Usage:
==============
#### Quick start:
Clone repo and execute the following command from terminal in repo folder. _git bash is recommended_

	vagrant box add ubuntu/trusty64
    vagrant up
Vagrant will download latest ubuntu stable and apply all scripts from bootstrap.sh. 
Now try to go to http://localhost:8080 to ensure wildfly is running.

#### SSH:
To SSH into the box and have a look around use

    vagrant ssh

#### Stopping:
To gracefully shutdown your vm use:

    vagrant halt

#### Reprovision:
The provisioning only runs the first time you start the vm, so in order to reprovision you can either run

    vagrant destroy --force
or use

    vagrant provision
Its recommended to use the destroy command between each run during development of the image, but the goal is to be able to simply run vagrant provision to reprovision the virtual machine.


#### Modifying:
The provisioning of the machine can be edited.
    
```bootstrap.sh```: This is where all software is installed on the virtual machine.
Here you can edit passwords and naming for your server. (Note that this is intended for dev-environment. The password is stored in clear text. Theese services are not intended to be exposed to internet)

```bootstrap.sql```: If you need to source your database with data at launch, you can add sql here.

```Vagrantfile```: The vagrant image settings (contains port forwarding and other settings revolving the virtual machine)

```wildfly-install.sh```: This is the file that takes care of wildfly installation. Lives in its own script, since the process is rather complicated. You do not need to edit this file unless you want to change wildfly configuration.

```data/wildfly_configuration/```: here are wildfly configuration files. The one you choose in ```wildfly-install.sh``` will be copied at each provision.