Vagrant.configure(2) do |config|
  ## Base image
  config.vm.box = "ubuntu/trusty64"
  config.vm.box_url = "https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"
  
  ## Provisioning script
  config.vm.provision :shell, path: "bootstrap.sh"
  
  ## You have two options to access wildfly living on the vagrant server.
  ## 1: Use port forwarding, and have the ports forwarded to your host machine.
  ## 2: Use a private network between the guest and the host.
  ##
  ## Both have its benefits and drawbacks:
  ## (OPTION 1)
  ## If you use port forwarding you can easily acces wildfly at localhost:8080 in your browser,
  ## as if it was running on your local machine. This also means that IF your machine has its ports exposed to the internet,
  ## so will wildfly. (if you are behind a router, in most cases you will not have your ports exposed to the internet)
  ## (OPTION 2)
  ## If you use a private network you can specify a static IP or use dhcp. 
  ## This wil not be accesible to any other computer than the host machine.
  
  ## Port forwarding
  config.vm.network "forwarded_port", guest: 8080, host: 8080
  config.vm.network "forwarded_port", guest: 9990, host: 9990
  ## if you need direct access to mysql uncomment below:
  #config.vm.network "forwarded_port", guest: 3306, host: 3306
  
  ## Private network (choose dhcp or static)
  #config.vm.network "private_network", ip: "192.168.0.165"
  #config.vm.network "private_network", type: "dhcp"
  
  ## If needed: - share an additional folder to the guest VM.
  ## vagrant already share the base vagrant folder to /vagrant on the guest machine.
  # config.vm.synced_folder "data", "/vagrant_data"

end