## here is where you put your SQL commands, if you need to bootstrap your database.
CREATE TABLE person
(
  id         INT PRIMARY KEY   NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(64)       NOT NULL,
  last_name  VARCHAR(64)       NOT NULL
)
  ENGINE = INNODB;
  
INSERT INTO person (`first_name`, `last_name`) VALUES ('Bill', 'Gates');